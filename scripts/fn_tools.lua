-- SOME VERY SPECIFIC FUNCTIONS

function _G.dw_tools_get_info_cursor()
	local ln = vim.fn.line('.') --Return the current line number
	local cn = vim.fn.col('.')  --Return the current column byte index
	local lt = vim.fn.line('$') --Return final last line number
	return ln, cn, lt
end

function _G.dw_tools_get_info_syntax()
	local function attr_name(synID)
		return vim.fn.synIDattr(synID, 'name')
	end

	local function synID_cursor(n)
		ln, cn = dw_tools_get_info_cursor()
		return vim.fn.synID(ln, cn, n)
	end

	local hi = attr_name(synID_cursor(1))
	local trans = attr_name(synID_cursor(0))
	local lo = attr_name(vim.fn.synIDtrans(synID_cursor(1)))
	return 'hi<'..hi..">\ntrans<"..trans..">\nlo<"..lo..">"
end

