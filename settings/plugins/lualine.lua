-- Lualine: config file
-- 
--		Can only pass anonymous lua functions inside objects,
--		if not you'll mess up the display of message errors,
--		but you can pass objects defined in variables, or use
--	 	a function that returns that variable
--
--	 	Also, is notable that extension don't support quite
--	 	well inactive_sections, so be carefull
--

--		For a cleaner setup i'll define this local objects
--		*	Here will be alternative options
local opts = {
	var = {
		paste = function()
			return vim.opt.paste:get() and '[paste]' or ''
		end,
		modified = function(minimal)
			minimal = minimal or false
			return not(vim.opt.mod:get()) and '' or (
			minimal and '[+]' or '[modified]')
		end,
		modifiable = function(minimal)
			minimal = minimal or false
			return vim.opt.ma:get() and '' or (
			minimal and '[-]' or '[no modifiable]')
		end
	},
	obj = {
		mode = function()
			return { 'mode', fmt = function(x)
				return x:sub(1,1) end
			}
		end
	}
}
--		*	Here will be sub-section definitions
local line = {
	full = {
		a = {'mode', function() return opts.var.paste() end },
		b = {'branch', 'diff', 'diagnostics'},
		c = { function() return '%t '.. opts.var.modified()..'%r' end },
		x = { function() return opts.var.modifiable() end },
		y = {'filetype'},
		z = {'%l:%v  %L'}
	},
	minimal = {
		a = { opts.obj.mode(), function() return opts.var.paste() end },
		b = {'branch'},
		c = { function() return '%t '.. opts.var.modified(true)..'%r' end },
		x = { function() return opts.var.modifiable(true) end },
		y = {'filetype'},
		z = {'%l:%v'}
	}
}

--	Current lualine setup
require('lualine').setup {
	options = {
		icons_enabled = false,
		theme = 'auto',
		component_separators = { left = '', right = ''},
		section_separators = { left = '', right = ''},
		disabled_filetypes = {},
		always_divide_middle = true,
	},
	sections = {
		lualine_a = line.full.a,
		lualine_b = line.full.b,
		lualine_c = line.full.c,
		lualine_x = line.full.x,
		lualine_y = line.full.y,
		lualine_z = line.full.z
	},
	inactive_sections = {
		lualine_a = line.minimal.a,
		lualine_b = line.minimal.b,
		lualine_c = line.minimal.c,
		lualine_x = line.minimal.x,
		lualine_y = line.minimal.y,
		lualine_z = line.minimal.z
	},
	tabline = {},
	extensions = { 
		{
			sections = {
				lualine_a = line.minimal.a,
				lualine_b = line.minimal.y,
				lualine_c = line.minimal.c,
				lualine_x = line.minimal.x,
				lualine_y = line.minimal.z,
				lualine_z = line.minimal.b
			},
			filetypes = {'help'}
		},
		{
			sections = {
				lualine_a = line.minimal.a,
				lualine_b = line.minimal.y,
				lualine_c = {},
				lualine_x = line.minimal.x,
				lualine_y = line.minimal.z,
				lualine_z = {}
			},
			filetypes = {'vim-plug'}
		},
	}
}
