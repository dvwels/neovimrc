"	SUB SECTION: Lightline settings
set noshowmode "Don't show MODE anymore in below the status line
let g:lightline = {
      \ 'colorscheme': 'srcery_drk',
	  \ 'active': {
	  \   'left': [['mode', 'paste'], ['gitbranch', 'readonly', 'filename', 'modified']],
  	  \   'right': [['lineinfo'], ['fileline'], ['fileformat', 'fileencoding', 'filetype']]
	  \	},
	  \ 'inactive': {
	  \   'left': [['mode']],
	  \   'right': [['lineinfo'], []]
	  \	},
	  \ 'component': {
	  \   'fileline': "%{'L:' . line('$')}",
	  \	},
	  \ 'component_function': {
	  \    'mode': 'LightlineModePlus'
	  \ },
      \ }

function! LightlineModePlus() abort
	let ftmap = {
				\ 'help': 'HELP',
				\ 'vim-plug': 'VIM-PLUG',
				\ }
	return get(ftmap, &filetype, lightline#mode())
endfunction

function! LightlineReload()
	call lightline#init()
	call lightline#colorscheme()
	call lightline#update()
endfunction
command! LightlineReload call LightlineReload()

