-- Bufferline: config file
--
-- //Normal will be the background of the current buffer
-- TabLineSel is for the current buffer
-- Comment are for non-current buffers
--

require('bufferline').setup {
	options = {
		numbers = function(x)
			return string.format('%s', x.id)
		end,
		indicator_icon = '',
		buffer_close_icon = '',
		modified_icon = '●',
		close_icon = '',
		left_trunc_marker = '',
		right_trunc_marker = '',
		name_formatter = function(x)
			return vim.fn.fnamemodify(x.name, ':t:r')
		end,
		diagnostics = 'nvim_lsp',
		--offsets = {
		--	{
		--		filetype = "NvimTree",
		--		text = "File Explorer"| function ,
		--		text_align = "left" | "center" | "right"
		--	}
		--},
		show_buffer_icons = true,
		show_buffer_close_icons = true,
		show_close_icon = false,
		show_tab_indicators = true,
		separator_style = 'thick',
		enforce_regular_tabs = false,
		always_show_bufferline = true,
		sort_by = 'tabs'
	}
}

