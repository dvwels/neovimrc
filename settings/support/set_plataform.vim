" Based On Plataforms: set options you'll be happy with
"
"	It's possible that some terminals or GUI plataforms
"		may have problems with some options (eg. color
"		support, not recognized keys)
"

"	If Run On: TTY does not support more than 16 colors
"
if &term == 'linux'
	let g:truecolor=0
	set notermguicolors
	colo elford " Color schemes will be limited to 4bit color

"	If Run On: Terminal emulators or GUIs
else
	let g:truecolor=1

"		True color, no limitation for colorschemes
	if has('termguicolors')
		set termguicolors

"		256 colors, limited but 8bit color is OK
	else
		echo 'This terminal may not support true color' .
					\ 'but may support 256 colors'
	endif
endif

