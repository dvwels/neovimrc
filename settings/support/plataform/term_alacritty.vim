"ALACRITTY SETTINGS:

if &term == 'alacritty'
"	SECTION: Patchs
"	When Vim runs on Alacritty, some
"		combinations of keys don't work
"	Don't know if the problem persist here
"		I'll prove later
"		ARROW KEYS WITH ALT:
	set <M-w>=w		"Alt + w
	set <M-k>=[1;3A	"Alt + k
	set <M-j>=[1;3B	"Alt + j
"		ARROW KEYS WITH CTRL:
"		FOR: Ctrl-Keys
	set <M-l>=[1;5C	"Alt + l
	set <M-h>=[1;5D	"Alt + h

"		RE REMAPPING KEYS:
	nnoremap <M-l> <C-Right>
	inoremap <M-l> <C-Right>
	nnoremap <M-h> <C-Left>
	inoremap <M-h> <C-Left>
endif


