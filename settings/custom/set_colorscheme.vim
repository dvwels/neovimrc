"CUSTOM CHANGES TO COLOR SCHEMEs:
"
"	This part is entirly personal, and 'cause of that
"		I'm not giving applying these changes to cterm
"
"
" 'eldar'        " Change Lualine section B* : Tabline theme not supported
" 'onedark'      " Change Lualine section B* : Tabline theme not supported
" 'monokai&pro'  " Change Lualine section B* : Tabline theme not supported
" 'papercolor'   " Change Lualine section B* : Tabline theme not supported
" 'gruvbox8$all' " Maybe Lualine* : Tabline theme not supported
" 'srcery'       " Maybe Lualine* : Tabline theme not supported
" 'badwolf'      " Maybe C and X Lualine* : Tabline theme not supported
"
" * I don't think it's possible to change
" 		lualine theme while preserving auto
" 		unless I write my own `auto`
"

"	First at all, configure your themes
let g:tokyonight_style='night'
let g:tokyonight_italic_functions = 1

"	ALL: should work for all dark colors schemes

function! Dw_Cs_FixAll() abort
	hi CursorLineNr    gui=italic
	hi CursorLine      guibg=#202020
	hi vimCommentTitle gui=italic,bold
	hi Comment         gui=italic
endfunction

"	BY COMPONENT: specific changes to specific colors schemes

function! Dw_Cs_FixCursorLine() abort
	hi CursorLine guibg=#171717
endfunction

"	SPECIFIC: specific changes to specific colors schemes

function! Dw_Cs_FixDracula() abort
	hi Normal     guibg=#202020
	hi CursorLine guibg=#2D2D2D
endfunction

function! Dw_Cs_FixEldar() abort
	hi CursorLine guibg=#101010
	hi Visual     guibg=#333333
	hi SignColumn guibg=#202020
"	hi StatusLine gui=bold guibg=#202020
"	hi WildMenu   guifg=#333333
"	hi TabLine   guifg=#333333
"	hi TabLineFill link TabLine
endfunction

function! Dw_Cs_FixOnedark() abort
	hi Normal     guibg=#202020
	hi CursorLine guibg=#2D2D2D
	hi Visual     guibg=#333333
endfunction

function! Dw_Cs_FixMonokais() abort
	hi LineNr guibg=#202020
endfunction

function! Dw_Cs_FixBadwolf() abort
"	hi Visual     guibg=#333333
"	hi SignColumn guibg=#202020
"	hi StatusLine gui=bold guibg=#343434
"	hi WildMenu   guifg=#333333
	hi DiffDelete guifg=#ff2c4b
endfunction

"	To avoid defining autocmds twice use augroup
augroup Dw_Cs_Fix
	autocmd!
	autocmd ColorScheme *
				\ call Dw_Cs_FixAll()

	autocmd ColorScheme dracula
				\ call Dw_Cs_FixDracula()
	autocmd ColorScheme eldar
				\ call Dw_Cs_FixEldar()
	autocmd ColorScheme onedark 
				\ call Dw_Cs_FixOnedark()
	autocmd ColorScheme badwolf
				\ call Dw_Cs_FixBadwolf()

	autocmd ColorScheme monokai,monokai_pro
				\ call Dw_Cs_FixMonokais()
	autocmd ColorScheme codedark,gruvbox8_hard
				\ call Dw_Cs_FixCursorLine()
augroup end

