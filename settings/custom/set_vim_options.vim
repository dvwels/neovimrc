" VIM SETTINGS: 

"	Override Neovim Defaults:

set noruler          " Avoid display line and column number
set noshowmode       " Avoid display --mode in command line
set wildoptions-=pum " Avoid vertical pop-up menu
"set shortmess+=F    " DEF NVIM: No file info while editing

"	General Behavior:

set updatetime=50       " Writes the swap file after Xms of inactivity
set completeopt=menuone " Properties of ins-completion pop-up menu

"	Split Behavior:

set splitright " When split, cursor go to right
set splitbelow " When vsplit, cursor goes down

"	Scrolling Behavior:

set mouse=a     " Supossed to make scroll work
set scrolloff=3 " Supossed to make scroll work

"	About Indentation:

"set tabstop=4    " Indent 4 spaces when manual <Tab>
"set shiftwidth=4 " Indent 4 spaces when use '>' or '<' for <Tab>
"set noexpandtab  " IF SET: Fill with tab, otherwise with spaces

"	I'd want to use tabs always but would cause
"		problems in some languages like Python

"	About Searching:

set ignorecase " Turn off case sensitive searching
set smartcase  " Turn on case sensitive when uppercase letters

"	About Line Number:

set number         " Display line number
set relativenumber " Uses relative numbers

"	Appearance:

set cursorline     " Highlight the line your cursor is in
set colorcolumn=80 " Highlight the column you set

