" PLUGIN MANAGER: VIM-PLUG

call plug#begin('~/.config/nvim/bundle')     " Here ur plugins will be stored
Plug 'junegunn/vim-plug', {'on':'help plug'} " Getting help page for vim-plug

"	General Purpose:

Plug 'kyazdani42/nvim-web-devicons' " Icons for plugins
Plug 'nvim-lualine/lualine.nvim'    " Customizable status line
Plug 'akinsho/bufferline.nvim'      " Customizable buffer/tab line
"Plug 'RRethy/vim-hexokinase'       " Color highlightning with Go
Plug 'dstein64/vim-startuptime', {'on':'StartupTime'}

"	Easy Writing:

Plug 'windwp/nvim-autopairs'            " More than a autopair mapping
Plug 'junegunn/vim-easy-align'          " Autoalign text based on delimiter keys
Plug 'junegunn/goyo.vim', {'on':'Goyo'} " Free-distraction mode

"	Improving NeoVIM:
Plug 'tversteeg/registers.nvim' " Visualize registers on pop-up menu


"	Development Tools:

Plug 'neovim/nvim-lspconfig'         " Enables built-in LSP support
Plug 'nvim-lua/plenary.nvim'         " Lua library for Neovim, is a dependency
Plug 'nvim-telescope/telescope.nvim' " More than a fuzzy finder


"	Language Specific:

"Plug 'ap/vim-css-color'

"	Software Required:

"Plug 'mcchrish/nnn.vim'
"Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'

"	Color Schemes:

Plug 'dracula/vim', {'as':'cs/dracula'}
Plug 'agude/vim-eldar', {'as':'cs/eldar'}
Plug 'sickill/vim-monokai', {'as':'cs/monokai'}
Plug 'phanviet/vim-monokai-pro', {'as':'cs/monokaipro'}
Plug 'NLKNguyen/papercolor-theme', {'as':'cs/papercolor'}
Plug 'nanotech/jellybeans.vim', {'as':'cs/jellybeans'}
Plug 'tomasiser/vim-code-dark', {'as':'cs/codedark'}
Plug 'lifepillar/vim-gruvbox8', {'as':'cs/gruvbox8'}
Plug 'srcery-colors/srcery-vim', {'as':'cs/srcery'}
Plug 'joshdick/onedark.vim', {'as':'cs/onedark'}
Plug 'sjl/badwolf', {'as':'cs/badwolf'}

"		This is a Lua colorscheme
"		See :h tokyonight.nvim-⚙️-configuration
Plug 'folke/tokyonight.nvim', {'as':'cs/tokyonight'}

"	That's all right?"
call plug#end()

