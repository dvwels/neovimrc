" VIM REMAPPINGS:
"
"	Be in mind that VIM has a ton of ways to remap, so this
"		file is a little bit messy and not all mappings can
"		be found here (eg. keybinding to plugins)
"
"	A quick explanation of ...
"		'nore'     : it uses defaults keys, non recursive
"		'<silent>' : it doesn't show up on command line
"		'<Cmd>'    : it doesn't enter to command mode
"
"	Notes:
"
"		:: 'unmap' does not turn off defaults bindings
"
"		:: '<Nop>' turns off a key binding, but be careful
"			cause order is important to avoid messing up
"			other previous mapped keybindings {1}
"		
"	{1}	Map [x] to <Nop>, then map [y] to [x]. Note that the
"			disabling will only work for the mode you specify.
"			This means, if you use `nnoremap [x] <Nop>`, then
"			you can use [x] only when mapping [lhs] to [rhs]
"			and use [y] with recursive mappings ([mode]map)
"

runtime settings/custom/mappings/mod_map.vim
runtime settings/custom/mappings/command_map.vim
runtime settings/custom/mappings/normal_map.vim
runtime settings/custom/mappings/insert_map.vim
runtime settings/custom/mappings/visual_map.vim



" INI SECTION: PLUGIN REMAPPING KEYS
vmap ga <Plug>(EasyAlign)
" END SECTION: PLUGIN REMAPPING KEYS

