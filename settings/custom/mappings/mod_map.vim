" MOD KEY MAPPINGS:

"	Alt Keys:
"		To handle panel movement
tnoremap <M-w> <C-w>
nnoremap <M-w> <C-w>
vnoremap <M-w> <Esc> <C-w>
inoremap <M-w> <Esc> <C-w>

"		To move line with cursor
nnoremap <M-Up> :m .-2<CR>==
vnoremap <M-Up> :m '<-2<CR>gv=gv
inoremap <M-Up> <Esc>:m .-2<CR>==gi
nnoremap <M-Down> :m .+1<CR>==
inoremap <M-Down> <Esc>:m .+1<CR>==gi
vnoremap <M-Down> :m '>+1<CR>gv=gv

