" NORMAL MODE MAPPINGS:

"	Key To Key:

"		Select all
nnoremap <C-a> ggVG

"		Switching behaviour
nnoremap P p
nnoremap p P
nnoremap <C-o> <C-i>
nnoremap <C-i> <C-o>

"		For consistency
nnoremap U <C-r>

"	Key To Command:

"	Clears hlsearch and the command line
nnoremap <Space> :nohlsearch<Bar>:echo<CR>

"	Default mapping in NVIM, dont understan yet
"nnoremap <C-L> <Cmd>nohlsearch<Bar>diffupdate<CR><C-L>


" WITH SPECIAL KEY: ¿, like g but with ¿
" 	
" 	If you want to use ¿ on mappings, use
" 		recursive mappings ([mode]map)
"

"		Enter Select Mode
nnoremap gh <Nop>
nnoremap gH <Nop>
nnoremap g<C-h> <Nop>

nnoremap ¿s gh
nnoremap ¿S gH
nnoremap ¿<C-s> g<C-h>

"		Enter Replace Mode
nnoremap R <Nop>
nnoremap gR <Nop>

nnoremap ¿r R
nnoremap ¿R gR

"	Toogle (on and off) relative numbers
nnoremap ¿¿ <Cmd>set relativenumber!<CR>

" OTHER EDITORS LIKE KEYBINDING:
"
"	Buffers Keybindings:
"
nnoremap <C-b> <Nop>
nnoremap <C-b><C-b> :buffers<CR>:buffer<Space>
nnoremap <C-b><C-d> :buffers<CR>:bdelete<Space>
nnoremap <C-b><C-x> <Cmd>bdelete<CR>

"	VSCode Like Keybindings:

nnoremap <C-k> <Nop>
nnoremap <C-k><C-t> :colo<Space>

