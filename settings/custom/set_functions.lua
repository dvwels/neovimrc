-- FUNCTIONS:
--
--	Only defintion of a function
--

function _G.dw_set_colorscheme()

	--Planning to show only the Color Schemes
	-- installed by plugin 
	return 'No done yet'
end

function _G.dw_get_fileinformation()
	--g:colors_name gives :colo<CR>
	local ft = vim.opt.ft:get() --filetype
	local ff = vim.opt.ff:get() --fileformat
	-- Neovim always uses utf-8 for encoding
	local fe = vim.opt.fileencoding:get()
	return ' '..ff..' | '..fe..' | '..ft
end

