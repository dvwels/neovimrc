" VIMRC: Well, actually is init.vim for Neovim

"	Quick access method
command! Config :e $MYVIMRC

"	Main Settings:

"	Load your plugins
runtime settings/custom/set_vim_plug.vim

"	Load vim options for os
runtime settings/support/set_os.vim

"	Load vim options for plataform {1}
runtime settings/support/set_plataform.vim

"	Load your vim options
runtime settings/custom/set_vim_options.vim

"	Load your color schemes
if g:truecolor  " Needs {1} to be run
	runtime settings/custom/set_colorscheme.vim
	colo srcery " Select your color scheme here {2}
endif
"
"	{1} 'Plataforms' needs to be run in order to get g:truecolor
"	{2} Color scheme on TTY is defined inside 'Plataforms' file
"

"	Remappings vim keys
runtime settings/custom/set_mappings.vim

"	Defining functions in Lua
runtime scripts/fn_tools.lua
runtime settings/custom/set_functions.lua
"	Mapping functions to Vim keys
runtime settings/custom/mappings/fn_map.vim

"	Plugin Settings:

"	TOOGLE TIMEOUTLEN: Reduce the wait time when press Leader Key
"nnoremap <silent> <Leader> :<C-U>set timeoutlen=2000<CR>:call feedkeys('<Leader>')<CR>
"	Select your <Leader> key
let mapleader='ñ'

runtime settings/plugins/netrw.vim
runtime settings/plugins/goyo.vim

runtime settings/plugins/autopairs.lua

runtime settings/plugins/lualine.lua
runtime settings/plugins/bufferline.lua

nnoremap <C-b><C-b> <Cmd>Telescope<Space>buffers<CR>
runtime settings/plugins/telescope.lua

runtime settings/plugins/lspconfig.vim

