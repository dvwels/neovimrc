" VIM INDENTATION SETTINGS:

set tabstop=4    " Indent 4 spaces when manual <Tab>
set shiftwidth=4 " Indent 4 spaces when use '>' or '<' for <Tab>
"set noexpandtab  " IF SET: Fill with tab, otherwise with spaces

